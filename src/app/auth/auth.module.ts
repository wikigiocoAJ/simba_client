import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {SsapModule} from '../ssap/ssap.module';
import {SsapService} from '../ssap/services/ssap.service';
import {AuthService} from './services/auth.service';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    SsapModule,
    TranslateModule
  ],
  declarations: []
})
export class AuthModule { }
