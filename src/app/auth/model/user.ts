import {Login, PayloadElement, SLogin} from './slogin';

export class User {
  username: string;
  roles: string[];
  loginTime: string;
  sessionId: string;

  setLoginResponse(username: string, loginResp: SLogin) {
    const obj: PayloadElement = <PayloadElement> loginResp.Payload[0];
    const userInfo: Login = <Login> obj.Login;

    this.username = username;
    this.roles = userInfo.Roles;
    this.loginTime = userInfo.ServerTs;
    this.sessionId = loginResp.Identification.SessionId;
  }

}
