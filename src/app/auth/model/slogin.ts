import {Identification} from '../../ssap/model/ssap';

export interface SLogin {
  Identification: Identification;
  Payload?: (PayloadElement)[] | null;
}

export interface PayloadElement {
  Login: Login;
}

export interface Login {
  Roles?: (string)[] | null;
  SessionCookie: boolean;
  Result: number;
  ServerTs: string;
}
