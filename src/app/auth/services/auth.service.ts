import {Injectable} from '@angular/core';
import {SsapService} from '../../ssap/services/ssap.service';
import {environment} from '../../../environments/environment';
import {SLogin} from '../model/slogin';
import {ResultCode} from '../../ssap/model/ResultCode';
import {User} from '../model/user';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class AuthService {
  authService = environment.authService;
  userLogged: User;
  static getLoginMessage(username: string, password: string) {
    return {'Login': {
      'SessionCookie': false,
        'Id': username,
        'Password': password
    }};
  }
  static getLogoutMessage() {
    return {'Logout': {}};
  }
  setUserLogged(userLogger: User) {
    this.userLogged = userLogger;
  }
  public getUserLogged() {
    return this.userLogged;
  }
  constructor(private ssapService: SsapService, private translate: TranslateService) {
    translate.setDefaultLang('en');
    translate.addLangs(['en', 'it']);
    translate.setDefaultLang('en');

    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|it/) ? browserLang : 'en');
  }

  login(username: string, password: string): Promise<ResultCode> {

    const logMsg = AuthService.getLoginMessage(username, password);
    return new Promise((resolve, reject) => {
      this.ssapService.sendMessage('Login', this.authService, logMsg).then(response => {
        // SUCCESS
        console.log(JSON.stringify(response));  // this is working
        const loginResp = <SLogin>response;
        const user = new User();
        user.setLoginResponse(username, loginResp);
        this.setUserLogged(user);

        resolve(new ResultCode(0, 'OK'));
      }, error => {
        // FAILURE
        console.log(error);
        reject(new ResultCode(-1, 'LOGIN ERROR', null, error));
      })
    });
  }

  logout(): Promise<ResultCode> {

    const logMsg = AuthService.getLogoutMessage();
    return new Promise((resolve, reject) => {
      this.ssapService.sendMessage('Logout', this.authService, logMsg, this.userLogged.sessionId).then(response => {
        // SUCCESS
        console.log(JSON.stringify(response));  // this is working
        resolve(new ResultCode(0, 'OK'));
      }, error => {
        // FAILURE
        console.log(error);
        reject(new ResultCode(-1, 'LOGIN ERROR', null, error));
      })
    });

  }
}
