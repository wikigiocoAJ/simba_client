export class Price {
  currency: string;
  value: number;

  constructor(_currency: string, _value: number) {
    this.currency = _currency;
    this.value = _value;
  }
}
