import {Property} from './property';

export class Properties {
  title: string | null;
  property: Property = new Property(true);
  properties: Properties[] = [];

  public isEquals(_prop: Properties): boolean {
    return  (this.title!==undefined && _prop.title === this.title) || (this.property!==undefined && this.property.isEquals(_prop));
  }
  constructor( _title?: string | null) {
    this.title = _title;
  }
  public isValid(): boolean {
    return this.title !==undefined || (this.property!== undefined && this.property.isValid());
  }

  public addProperty(_property: Properties) {
    this.properties.push(_property);
  }

  public toObject(): object {
    if (this.title === undefined) { return null; }
    let str = toString();
    const obj = JSON.parse(str);
    return obj;
  }

  public toString(): string {
    let str = '';
    if (this.title !== undefined) {
      str += ' "' + this.title + '": {';
      let count = 0;
      this.properties.forEach((prop) => {
        if (count !== 0) str += ','
        str += prop.toString();
      });
      str += ' }';
    } else {
      str += this.property.toString();
    }
    return str;
  }
}
