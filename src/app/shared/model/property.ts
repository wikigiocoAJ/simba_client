
export class Property {
  constructor(parent?: (boolean) | null) {
    this.parent = parent;
  }
  name?: (string) | null;
  value?: (string) | null;
  number?: (boolean) | null;
  parent?: (boolean) | null;
  public toString(): string  {
    let string = '"'+this.name + '": ';
    if(this.number) {
      string += this.value ;
    }
    else {
      string += '"' + this.value + '"';
    }
    return string;
  }
  public isValid(): boolean {
    return this.name !== undefined && this.value !== undefined;
  }
  public isEquals(_prop: Property): boolean {
    return  (this.name!==undefined && _prop.name=== this.name);
  }

}
