import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import {
  DynamicFormArrayModel,
  DynamicFormControlModel,
  DynamicFormLayout,
  DynamicFormService,
  DynamicInputModel
} from '@ng-dynamic-forms/core';
import {HttpClient} from '@angular/common/http';
import {BOOTSTRAP_SAMPLE_FORM_MODEL} from './bootstrap-sample-form.model';
import {BOOTSTRAP_SAMPLE_FORM_LAYOUT} from './bootstrap-sample-form.layout';
import {environment} from '../../../../environments/environment';
import {container} from '@angular/core/src/render3';

@Component({
  selector: 'app-generic-form',
  templateUrl: './generic-form.component.html',
  styleUrls: ['./generic-form.component.scss']
})
export class GenericFormComponent implements OnInit {
  formUrl = environment.formService;

  @Input() context: string;
  @Input() name: string;
  @Input() title: string;

  @Output('onSubmit') object = new EventEmitter<string>();

  formModel: DynamicFormControlModel[] = BOOTSTRAP_SAMPLE_FORM_MODEL;
  formGroup: FormGroup;
  formLayout: DynamicFormLayout = BOOTSTRAP_SAMPLE_FORM_LAYOUT;

  exampleControl: FormControl;
  exampleModel: DynamicInputModel;

  arrayControl: FormArray;
  arrayModel: DynamicFormArrayModel;

  constructor(private formService: DynamicFormService, private http: HttpClient) {}

  ngOnInit() {

    /*  this.formGroup = this.formService.createFormGroup(this.formModel);*/
    this.loadJson();
    this.exampleControl = this.formGroup.get('bsFormGroup1').get('bsInput') as FormControl;
    this.exampleModel = this.formService.findById('bsInput', this.formModel) as DynamicInputModel;

    this.arrayControl = this.formGroup.get('bsFormGroup2').get('bsFormArray') as FormArray;
    this.arrayModel = this.formService.findById('bsFormArray', this.formModel) as DynamicFormArrayModel;

  }

  public loadJson() {
    const path_link = this.formUrl + '/' + this.context + '/form-' + this.name + '.json';
    console.log('-- loading ' + path_link);
    this.http.get<object[]>(path_link).subscribe(formModelJson => {
      console.log(JSON.stringify(formModelJson));
      this.formModel = this.formService.fromJSON(formModelJson);
      this.formGroup = this.formService.createFormGroup(this.formModel);
    });
  }
  add() {
    this.formService.addFormArrayGroup(this.arrayControl, this.arrayModel);
  }

  insert(context: DynamicFormArrayModel, index: number) {
    this.formService.insertFormArrayGroup(index, this.arrayControl, context);
    console.log(this.formModel);
  }

  remove(context: DynamicFormArrayModel, index: number) {
    this.formService.removeFormArrayGroup(index, this.arrayControl, context);
  }

  move(context: DynamicFormArrayModel, index: number, step: number) {
    this.formService.moveFormArrayGroup(index, step, this.arrayControl, context);
  }

  clear() {
    this.formService.clearFormArray(this.arrayControl, this.arrayModel);
  }

  test() {
    //this.exampleModel.disabledUpdates.next(!this.exampleModel.disabled);
    //this.exampleModel.valueUpdates.next('Hello Hello');
    //console.log(JSON.stringify(this.exampleModel));
    //this.arrayModel.get(1).group[0].valueUpdates.next('This is just a test');
    //this.formService.moveFormArrayGroup(2, -1, this.arrayControl, this.arrayModel);
    /*
    this.formService.addFormGroupControl(
        this.formGroup,
        this.formModel,
        new DynamicFormGroupModel({
            id: 'bsFormGroup3',
            group: [new DynamicInputModel({id: 'newInput'})]
        })
    );
    this.formService.addFormGroupControl(
        this.formGroup.get('bsFormGroup3') as FormGroup,
        this.formModel[2] as DynamicFormGroupModel,
        new DynamicInputModel({id: 'newInput'})
    );
    */
    //this.exampleModel.add({label: 'Option 5', value: 'option-5'});
  }

  saveSubmit() {
    let json: string = JSON.stringify(this.formGroup.value);
    console.log(json);
    this.object.emit(json);
    //this.formGroup.setValue(this.formGroup.value);
  }

  onBlur($event) {
    console.log(`BLUR event on ${$event.model.id}: `, $event);
  }

  onChange($event) {
    console.log(`CHANGE event on ${$event.model.id}: `, $event);
  }

  onFocus($event) {
    console.log(`FOCUS event on ${$event.model.id}: `, $event);
  }
}
