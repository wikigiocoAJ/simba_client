import { CommonModule } from '@angular/common';
import {AuthModule} from '../auth/auth.module';
import {SsapModule} from '../ssap/ssap.module';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BsDatepickerModule, TabsModule, TimepickerModule} from 'ngx-bootstrap';
import { GenericFormComponent } from './components/generic-form/generic-form.component';

import { NgbDatepickerModule, NgbRatingModule, NgbTimepickerModule } from '@ng-bootstrap/ng-bootstrap';
import {DYNAMIC_VALIDATORS, DynamicFormsCoreModule, DynamicFormService, Validator, ValidatorFactory} from '@ng-dynamic-forms/core';
import { DynamicFormsBootstrapUIModule } from '@ng-dynamic-forms/ui-bootstrap';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TabsModule,
    AuthModule,
    SsapModule,
    TranslateModule,
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    NgbDatepickerModule,
    NgbRatingModule,
    NgbTimepickerModule,
    DynamicFormsCoreModule,
    DynamicFormsBootstrapUIModule
  ],
  declarations: [GenericFormComponent],
  exports:[GenericFormComponent],
  providers: [DynamicFormService]
})
export class SharedModule { }
