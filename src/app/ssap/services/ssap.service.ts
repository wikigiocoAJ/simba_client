import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {catchError, map} from 'rxjs/operators';

@Injectable()
export class SsapService {
  // TODO GESTIRE CORRETTAMENTE ERRORI HTTP DA RISPOSTA NELLA CLASSE RESULT
  headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });
  static handleError(error: Response | any) {
    console.error('ApiService::handleError', error);
    if (error instanceof  HttpErrorResponse) {
      console.error(error.message);
    }
    return Observable.throw(error);
  }
  static getTransactionId(): number {
    const d = new Date();
    return d.getTime();
  }

  static getSSapHeader(body, sessionId?: string , transactionId?: number) {
    if (transactionId === undefined) {
      transactionId = SsapService.getTransactionId();
    }
    if (sessionId === undefined) {
      sessionId = '';
    }
    return {
      'Identification': {
        'ClientInfo': {
          'ClientType': 500,
          'TransactionId': '' + transactionId
        },
        'SessionId': sessionId
      },
      'Payload': [body]
    };
  }

  constructor(private http: HttpClient) {
  }

  sendMessage(messageName: string, url: string, body: object, sessionId?: string, transactionId?: number) {
    if (transactionId === undefined) {
      transactionId = SsapService.getTransactionId();
    }
    if (sessionId === undefined) {
      sessionId = '';
    }
    const body_json = JSON.stringify(SsapService.getSSapHeader(body, sessionId, transactionId));
    console.log('********* REQUEST ' + messageName + ' - ' + transactionId + ' **********');
    console.log('********* BODY =  ' + body_json);
    return new Promise((resolve, reject) => {
      this.http
        .post(url, body_json, {headers: this.headers})
        .pipe(
          map((response: Response) => {
            switch (response.status) {
              case 201 :
              case 200 :
                console.log('********* RESPONSE ' + messageName + ' - ' + transactionId + ' **********');
                console.log('********* RESP =  ' + JSON.stringify(response));
                return response;
            }
            console.log('********* RESPONSE ' + messageName + ' - ' + transactionId + ' **********');
            console.log('********* RESP =  ' + JSON.stringify(response));
            return response;
          }),
          catchError(SsapService.handleError)
        ).subscribe(data => {
        resolve(data)
      })
    });
  }
}


