import { TestBed, inject } from '@angular/core/testing';

import { SsapService } from './ssap.service';

describe('SsapService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SsapService]
    });
  });

  it('should be created', inject([SsapService], (service: SsapService) => {
    expect(service).toBeTruthy();
  }));
});
