export class ResultCode {
  code: number;
  message: string;
  exception: Error;
  isError: boolean;
  object: Object;
  constructor(code: number, message: string, object?: Object, exception?: Error) {
    this.code = code;
    this.message = message;
    this.object = object;
    if (exception === undefined) {
      this.isError = false;
    } else {
      this.exception = exception;
      this.isError = true;
    }
  }
  check(): boolean {
    return !this.isError;
  }
}
