export interface SSap {
  Identification: Identification;
  Payload?: (PayloadEntity)[] | null;
}
export interface Identification {
  ClientInfo: ClientInfo;
  SessionId: string;
}
export interface ClientInfo {
  ClientType: number;
  TransactionId: string;
}
export interface PayloadEntity {
 polimorfo
  result
}
