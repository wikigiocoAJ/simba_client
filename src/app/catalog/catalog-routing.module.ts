import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CatalogComponent} from './components/catalog/catalog.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Catalog'
    },
    children: [
      {
        path: 'catalog',
        component: CatalogComponent,
        data: {
          title: 'Catalog'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogRoutingModule {}
