import {Product} from './product';
import {GenericComponent} from '../../shared/model/generic-component';
import {Properties} from '../../shared/model/properties';

export class Category implements GenericComponent{
  context?: string = 'ECommerce';
  description: string;
  categories?: (string)[] | null;
  tags?: (string)[] | null;
  properties?: Properties[] = [];
  products: Product[] | null;
  jsonObject?: string | null;
  constructor( _description?: string, _categories?: (string)[] | null, _tags?: (string)[] | null, _properties?: Properties[] | null) {
    this.description = _description;
    this.categories = _categories;
    this.tags = _tags;
    if (_properties !== undefined) {
      this.properties = _properties;
    }
  }
  public addProduct(product: Product) {
    this.products.push(product);
  }
  public hasCategories(): boolean {
    return this.categories !== undefined && this.categories.length > 0;
  }
  public hasTags(): boolean {
    return this.tags !== undefined && this.tags.length > 0;
  }
  public addProperties(_properties: Properties){
    this.properties.push(_properties);
  }
  public getPropertiesObject(): object {

    const obj = JSON.parse(this.toStringProperties());
    return obj;
  }
  public toStringProperties(): string {
    let str = '';
    str = '{' ;

    this.properties.forEach((prop) => {
      str += prop.toString();
    });
    str += '}';
    console.log(str);
    return str;
  }
  isNew(): boolean {
    return (this.description === undefined);
  }

}
