import {Price} from '../../shared/model/price';

export class Product {
  salable: boolean;
  description: string;
  price: Price;
  id: string;
  properties: object;
  constructor(_salable: boolean, _description: string, _price: Price,  _id: string, _properties: object) {
    this.salable = _salable;
    this.description = _description;
    this.id = _id;
    this.properties = _properties;
    this.price = _price;
  }
}
