import {Identification} from '../../ssap/model/ssap';

export interface SsapCategories {
  Identification: Identification;
  Payload?: (PayloadEntity)[] | null;
}
export interface PayloadEntity {
  GetTagCategories: GetTagCategories;
}
export interface GetTagCategories {
  More: boolean;
  Subset: number;
  Categories?: (string)[] | null;
  TagCategoryList?: (TagCategoryListEntity)[] | null;
  SubsetLength: number;
  Result: number;
}
export interface TagCategoryListEntity {
  Description: string;
  Categories?: (string)[] | null;
  Tags?: (string)[] | null;
  Properties?: Properties | null;
}

export interface Properties {
  Status: string;
  Weather: string;
  Ground: string;
}

