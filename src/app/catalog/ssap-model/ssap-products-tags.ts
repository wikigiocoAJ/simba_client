import {Identification} from '../../ssap/model/ssap';

export interface SsapProductsTags {
  Identification: Identification;
  Payload?: (PayloadEntity)[] | null;
}

export interface PayloadEntity {
  GetProductsByTags: GetProductsByTags;
}
export interface GetProductsByTags {
  ProductList?: (ProductListEntity)[] | null;
  More: boolean;
  Subset: number;
  Details: boolean;
  NextTags?: (string)[] | null;
  SubsetLength: number;
  Tags?: (string)[] | null;
  Result: number;
}
export interface ProductListEntity {
  Salable: boolean;
  Description: string;
  Price: Price;
  Id: string;
  Properties: Properties;
  Tags?: (string)[] | null;
}
export interface Price {
  Currency: string;
  Value: number;
}
export interface Properties {
  Status: string;
  Position: number;
  Outcome: Outcome;
  Race: Race;
  Horse: Horse;
  Driver: Driver;
  Id: string;
  Racetrack: Racetrack;
  Market: Market;
}
export interface Outcome {
  Position: number;
  Odd: number;
}
export interface Race {
  Status: string;
  Number: number;
  Prize: string;
  Datetime: string;
  Name: string;
}
export interface Horse {
  Number: number;
  Name: string;
}
export interface Driver {
  Number: number;
  Weight: number;
  Name: string;
}
export interface Racetrack {
  Nation: string;
  Speciality: string;
  Key: string;
  Name: string;
}
export interface Market {
  Status: string;
  BetType: string;
  BetContext: string;
}
