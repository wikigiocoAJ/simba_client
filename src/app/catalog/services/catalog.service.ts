import {Injectable} from '@angular/core';
import {SsapService} from '../../ssap/services/ssap.service';
import {AuthService} from '../../auth/services/auth.service';
import {environment} from '../../../environments/environment';
import {ResultCode} from '../../ssap/model/ResultCode';
import {SsapCategories, TagCategoryListEntity} from '../ssap-model/ssap-categories';
import {ProductListEntity, SsapProductsTags} from '../ssap-model/ssap-products-tags';
import {Product} from '../model/product';
import {Price} from '../../shared/model/price';
import {Category} from '../model/category';

@Injectable()
export class CatalogService {
  catalogService = environment.authService;

  static getHeaderCategories(category: string) {
    if (category === undefined) {
      return {
        'GetTagCategories': {
          'Subset': 1
        }
      };
    }
    return {
      'GetTagCategories': {
        'Subset': 1,
        'Categories': [category]
      }
    };
  }
  static msgAddCategory(category: Category) {

    let objProp = undefined;
    if(category.properties.length >0 ){
      objProp = category.getPropertiesObject();
    }


    return {
      'AddCategories': {
        'CategoryList': [
          {
            'Description': category.description,
            'Properties': objProp,
            'Tags': category.tags
          }
        ]
      }
    };
  }
  static msgAddCategoryJson(json: string) {

    let objProp = JSON.parse(json);


    return {
      'AddCategories': {
        'CategoryList': [
          objProp
        ]
      }
    };
  }
  static msgProductsTags(tags: string[]) {
    return {
      'GetProductsByTags': {
        'Subset': 1,
        'Details': true,
        'Tags': tags
      }
    };
  }

  static translateProduct(prodEnt: ProductListEntity) {
    const price = new Price(prodEnt.Price.Currency, prodEnt.Price.Value);
    return new Product(prodEnt.Salable, prodEnt.Description, price, prodEnt.Id, prodEnt.Properties);
  }
  static translateCategory(cat: TagCategoryListEntity) {
    /*  constructor( _description: string, _categories?: (string)[] | null, _tags?: (string)[] | null, _properties?: object | null) {*/
    // TODO DA PASSARE L'ARRAY DI PROPERTIES DALL OGGETO cat.Properties
    return new Category(cat.Description, cat.Categories, cat.Tags);
  }
  constructor(private ssapService: SsapService, private authService: AuthService) {
  }

  getCategories(category?: string): Promise<ResultCode> {
      const logMsg = CatalogService.getHeaderCategories(category);
    return new Promise((resolve, reject) => {
      const user = this.authService.getUserLogged();
      const sessionId = user.sessionId;
      this.ssapService.sendMessage('GetCategories', this.catalogService, logMsg, sessionId).then(response => {
        const catResp = <SsapCategories>response;
        const categoryList: Category[] = [];
        catResp.Payload[0].GetTagCategories.TagCategoryList.forEach( (cat) =>  {
          categoryList.push(CatalogService.translateCategory(cat));
        });
        console.log(JSON.stringify(response));  // this is working
        resolve(new ResultCode(0, 'OK', categoryList));
      }, function (error) {
        // FAILURE
        console.log(error);
        reject(new ResultCode(-1, 'LOGIN ERROR', null, error));
      })
    });
  }
  getProductsByTags(category: Category): Promise<ResultCode> {
    const logMsg = CatalogService.msgProductsTags(category.tags);
    return new Promise((resolve, reject) => {
      this.ssapService.sendMessage('GetProductsByTags', this.catalogService, logMsg,
                                    this.authService.userLogged.sessionId).then(response => {
        // SUCCESS
        const catResp = <SsapProductsTags>response;
        const productList: Product[] = [];
        catResp.Payload[0].GetProductsByTags.ProductList.forEach( (prod) =>  {
            category.addProduct(CatalogService.translateProduct(prod));
        });
        console.log(JSON.stringify(response));  // this is working
        resolve(new ResultCode(0, 'OK', category));
      }, function (error) {
        // FAILURE
        console.log(error);
        reject(new ResultCode(-1, 'LOGIN ERROR', null, error));
      })
    });
  }
  addCategory(category: Category): Promise<ResultCode> {
    const logMsg = CatalogService.msgAddCategory(category);
    return new Promise((resolve, reject) => {
      this.ssapService.sendMessage('AddCategory', this.catalogService, logMsg,
        this.authService.userLogged.sessionId).then(response => {

        console.log(JSON.stringify(response));  // this is working
        resolve(new ResultCode(0, 'OK', response));
      }, function (error) {
        // FAILURE
        console.log(error);
        reject(new ResultCode(-1, 'LOGIN ERROR', null, error));
      })
    });
  }
  addCategoryJSon(json: string): Promise<ResultCode> {
    const logMsg = CatalogService.msgAddCategoryJson(json);
    return new Promise((resolve, reject) => {
      this.ssapService.sendMessage('AddCategory', this.catalogService, logMsg,
        this.authService.userLogged.sessionId).then(response => {

        console.log(JSON.stringify(response));  // this is working
        resolve(new ResultCode(0, 'OK', response));
      }, function (error) {
        // FAILURE
        console.log(error);
        reject(new ResultCode(-1, 'LOGIN ERROR', null, error));
      })
    });
  }


  addProduct(product: Product): Promise<ResultCode> {
    const logMsg = CatalogService.msgAddProduct(product);
    return new Promise((resolve, reject) => {
      this.ssapService.sendMessage('AddProduct', this.catalogService, logMsg,
        this.authService.userLogged.sessionId).then(response => {

        console.log(JSON.stringify(response));  // this is working
        resolve(new ResultCode(0, 'OK', response));
      }, function (error) {
        // FAILURE
        console.log(error);
        reject(new ResultCode(-1, 'LOGIN ERROR', null, error));
      })
    });
  }

  private static msgAddProduct(product: Product) {
    return {
      'AddProducts': {
        'ProductList': [{
          'Salable': true,
          'Description': 'MIT - 1 - Win - 1',
          'Price': {
            'Currency': 'EUR',
            'Value': 200
          },
          'Properties': {
            'Racetrack': {
              'Name': 'Milano',
              'Speciality': 'Trotting',
              'Key': 'MIT',
              'Nation': 'it'
            },
            'Race': {
              'Number': 1,
              'Datetime': '200180101 10:00',
              'Name': 'Corsa 1',
              'Prize': 'Pr Lidia Tesio',
              'Status': 'active'
            },
            'Market': {
              'BetContext': 'Quotafissa',
              'BetType': 'Win',
              'Status': 'active'
            },
            'Horse': {
              'Number': 145,
              'Name': 'CAVALLO 1'
            },
            'Driver': {
              'Number': 3,
              'Weight': 59,
              'Name': 'DRIVER 1'
            },
            'Outcome': {
              'Position': 1,
              'Odd': 5681
            },
            'Position': 1,
            'Status': 'active'
          },
          'Id': '6b32699a-2c4f-413d-9c8f-d5e4456c4aa9',
          'Tags': ['Milano', 'Trotting', '1', 'Win']
        }]
      }
    };
  }
}
