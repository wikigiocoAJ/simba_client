import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CatalogService} from './services/catalog.service';
import {SharedModule} from '../shared/shared.module';
import { CatalogComponent } from './components/catalog/catalog.component';
import {CatalogRoutingModule} from './catalog-routing.module';
import { CategoryFormComponent } from './components/category-form/category-form.component';
import {FormsModule} from '@angular/forms';
import {TabsModule} from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    CatalogRoutingModule,
    FormsModule,
    TabsModule,
    SharedModule
  ],
  declarations: [CatalogComponent, CategoryFormComponent],
  providers: [CatalogService]
})
export class CatalogModule { }
