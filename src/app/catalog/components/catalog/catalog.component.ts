import {Component, OnInit} from '@angular/core';
import {Category} from '../../model/category';
import {CatalogService} from '../../services/catalog.service';
import {AuthService} from '../../../auth/services/auth.service';
import {Product} from '../../model/product';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent implements OnInit {
  categories: Category[] = [];
  products: Product[] = [];
  description: string;
  contextList = [{title:'ECommerce', key:'general'},{title:'Horse Betting', key:'races'},{title:'Others', key:'other'}];
  selectedCategory: Category = null;
  constructor(private catalogService: CatalogService) {
  }

  ngOnInit() {
    this.getCategories();
  }

  getCategories(category?: string, recursive?: boolean) {
    this.categories = [];
    this.catalogService.getCategories(category).then(response => {
        if (response.check()) {
          console.log(JSON.stringify(response));  // this is working
          const categories: Category[] = <Category[]>response.object;
          categories.forEach((tagCat) => {
              if (recursive) {
                if (tagCat.hasCategories()) {
                  tagCat.categories.forEach((cat) => {
                    this.getCategories(cat);
                  });
                }
                if (tagCat.hasTags()) {
                  this.getProductsByTags(tagCat);
                }
              } else {
                this.categories.push(tagCat);
              }
            }
          );
        }
      },
      errorCat => {
        console.log('error');
        console.error(errorCat);
      }
    );
  }
  newProduct: boolean = false;

  getProductsByTags(category: Category) {
    this.catalogService.getProductsByTags(category).then(response => {
      if (response.check()) {
        console.log(JSON.stringify(response));  // this is working
      }
    }, errorCat => {
      console.log('error');
      console.error(errorCat);
    });
  }
  onSelectCategory(_category: Category) {
    this.selectedCategory = _category;
  }
  addCategory(_context: string) {
    this.selectedCategory = new Category();
    this.selectedCategory.context = _context;
  }

  onSubmit($event) {

  console.log($event);

    this.catalogService.addCategoryJSon($event).then(response => {
        if (response.check()) {
          console.log(JSON.stringify(response));  // this is working
          this.selectedCategory = null;
         /* this.getCategories();*/
        }
      },
      errorCat => {
        console.log('error');
        console.error(errorCat);
      }
    );

  }

  addProduct() {
    this.newProduct = true;
  }
}
