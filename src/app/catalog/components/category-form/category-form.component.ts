import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Category} from '../../model/category';
import {Customer} from './customer';
import {Properties} from '../../../shared/model/properties';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.scss']
})
export class CategoryFormComponent implements OnInit {
  @Input('category') selectedCategory?: Category;
  @Output('addCategory')
  addCategoryEvent = new EventEmitter<Category>();

  ngOnInit() {
  }
  submitted = false;

  onSubmit() {
    this.addCategoryEvent.emit(this.selectedCategory);
    this.submitted = true;
  }

  addProperty($event: Properties) {
    this.selectedCategory.addProperties($event);
  }
  saveCategory(_event) {
    this.selectedCategory.jsonObject = _event;
    this.addCategoryEvent.emit(this.selectedCategory);
  }
}

