// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,
  authService: 'http://192.168.1.9:9001/fep1a',
  catalogService: 'http://192.168.1.9:9001/fep1a',
  sellingService: 'http://192.168.1.9:9001/fep1a',
  monitorService: ' http://192.168.1.9:9000/Monitor',
  formService: './assets/forms'
};
